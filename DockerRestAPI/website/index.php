<html>
    <head>
        <title>CIS 322 REST-api demo: Laptop list</title>
    </head>

    <body>
        <ul>
            <?php
            $uri = substr($_SERVER['REQUEST_URI'], 1, -1);
	    $uri = str_replace('/csv', '', $uri);
            $uri = str_replace('/json', '', $uri);
            $mode = str_replace('/?top=', '', $uri);
	    $top = $_GET['top'];

	    if ($mode == 'listAll' or $mode == 'listOpenOnly'){
	    echo 'Open Time'; 	   
	     if ($top == ''){
		$json = file_get_contents('http://brevet_web/'.$mode);
                $obj = json_decode($json);
                $opens = $obj->open_time;
                foreach ($opens as $l) {
                    echo "<li>$l</li>";
                }
	    } else {
	        $json = file_get_contents('http://brevet_web/'.$mode.'?top='.$top);
            	$obj = json_decode($json);
                $opens = $obj->open_time;
                foreach ($opens as $l) {
                    echo "<li>$l</li>";
                }
  	    }
	    }
	    if ($mode == 'listAll' or $mode == 'listCloseOnly'){
		echo 'Close Time';
	    if ($top == ''){
                $json = file_get_contents('http://brevet_web/'.$mode);
                $obj = json_decode($json);
                $closes = $obj->close_time;
                foreach ($closes as $l) {
                    echo "<li>$l</li>";
                }
            } else {
                $json = file_get_contents('http://brevet_web/'.$mode.'?top='.$top);
                $obj = json_decode($json);
                $closes = $obj->close_time;
                foreach ($closes as $l) {
                    echo "<li>$l</li>";
                }
            }

	    }
            ?>
        </ul>
    </body>
</html>
