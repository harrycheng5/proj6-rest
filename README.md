Author: Harry Cheng hcheng5@uoregon.edu

Repo: https://bitbucket.org/harrycheng5/proj6-rest/

Description: It is a web app that can compute the open time and close time for brevet ride based on the control points user has put in. For the rule of brevet, you can check here (https://rusa.org/pages/rulesForRiders).

# Project 6: Brevet time calculator service

Simple listing service from project 5 stored in MongoDB database.

## What is in this repository

You have a minimal implementation of Docker compose in DockerRestAPI folder, using which you can create REST API-based services (as demonstrated in class). 

## what does it do

It is improved version of web app that is based upon project 4 and 5. You can find the basic information here (https://bitbucket.org/harrycheng5/proj4-brevets/).

Besides the new function of two buttons (submit and display) from project, it now can creat some basic APIs and has consumer programs to show users those APIs.

## To use this program
clone this repo and pass it to your terminal. Change your directory to proj6-rest/DockerRestAPI and type $ docker-compose up, docker-compose.yml will build essential containers for you and launch the web app. You can then start using this program by enter http://localhost:5000/ in your browser.

## How to use new functionality
* To see the API of the data you have entered (json format as default):

    * "http://localhost:5000/listAll" should return all open and close times in the database
    * "http://localhost:5000/listOpenOnly" should return open times only
    * "http://localhost:5000/listCloseOnly" should return close times only

* In csv or json format: 

    * "http://localhost:5000/listAll/csv" should return all open and close times in CSV format
    * "http://localhost:5000/listOpenOnly/csv" should return open times only in CSV format
    * "http://localhost:5000/listCloseOnly/csv" should return close times only in CSV format

    * "http://localhost:5000/listAll/json" should return all open and close times in JSON format
    * "http://localhost:5000/listOpenOnly/json" should return open times only in JSON format
    * "http://localhost:5000/listCloseOnly/json" should return close times only in JSON format

* You can also specify how many row you want to see:

    * "http://localhost:5000/listOpenOnly/csv?top=3" should return top 3 open times only (in ascending order) in CSV format 
    * "http://localhost:5000/listOpenOnly/json?top=5" should return top 5 open times only (in ascending order) in JSON format
    * "http://localhost:5000/listCloseOnly/csv?top=6" should return top 5 close times only (in ascending order) in CSV format
    * "http://localhost:5000/listCloseOnly/json?top=4" should return top 4 close times only (in ascending order) in JSON format

To use consumer programs, please enter the URLs in the same format as above with change of host 5000 to 5001.
